# mypy

optional static typing for Python

* https://tracker.debian.org/pkg/mypy

## License
* https://github.com/python/mypy/blob/master/LICENSE

## See also
* [python-packages-demo/mypy](https://gitlab.com/python-packages-demo/mypy)
* Not yet [\*mypy\*](https://pkgs.alpinelinux.org/packages?name=*mypy*)@AlpineLinux